import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginService} from "./login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  register: FormGroup;
  formInvalid: boolean = false;
  constructor(private frmBuilder: FormBuilder, private loginService: LoginService) { }

  ngOnInit(){
    this.register = this.frmBuilder.group({
      username:["", [Validators.required]],
      password:["", [Validators.required]]
    });
  }

  onSubmit(){
    if(!this.register.valid) {
      this.formInvalid = true;
    }

    this.loginService.login(this.register.value)
      .subscribe(result => {
        if (result.success) {
          console.log("Login successful");
        }
      });
  }
}
